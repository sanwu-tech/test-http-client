NAME=$(shell basename $(shell pwd))
CC=gcc
override CFLAGS +=-Wall `pkg-config --cflags libsoup-2.4`
override LDFLAGS +=`pkg-config --libs libsoup-2.4`

all: main.cpp
	$(CC) $(CFLAGS) -o $(NAME) $? $(CFLAGS) $(LDFLAGS)

clean:
	-@rm $(NAME) *.o 2> /dev/null || true
