# Test HTTP Client
A test HTTP GET/POST client written in C/C++

## Requirements
1. glib-2.0
   On Ubuntu, run `apt-get install libglib2.0-dev`

2. libsoup-2.4
   On Ubuntu, run `apt-get install libsoup2.4-dev`

## Build
1. Run `make`

## Run
2. Run `./test-http-client`
