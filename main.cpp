#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <glib-2.0/glib.h>
#include <libsoup/soup.h>

static const size_t BUF_LEN = 10 * 1024 * 1024;
static char buffer[BUF_LEN];

static int get()
{
	SoupSession *session = NULL;
	SoupMessage *message = NULL;
	GInputStream *stream = NULL;
	GError *error = NULL;
	FILE *file = NULL;
	gsize nwrite = 0;
	gsize nread = 0;

	// Send HTTP GET request to server
	session = soup_session_new_with_options(SOUP_SESSION_ADD_FEATURE_BY_TYPE, SOUP_TYPE_CONTENT_SNIFFER, NULL);
	message = soup_message_new("GET", "http://localhost:8080/test.jpg");
	stream = soup_session_send(session, message, NULL, &error);
	if (error != NULL) {
		fprintf(stderr, "error: failed to send HTTP GET request: %s\n", error->message);
		g_error_free(error);
		goto out;
	}
	fprintf(stdout, "STATUS: GET: %d\n", message->status_code);

	if (message->status_code != 200) {
		fprintf(stderr, "error: failed to receive image from server\n");
		exit(-1);
	}

	// Read image data from response
	g_input_stream_read_all(stream, buffer, sizeof(buffer), &nread, NULL, &error);
	if (error != NULL) {
		fprintf(stderr, "error: failed to read HTTP GET response: %s\n", error->message);
		g_error_free(error);
		goto out;
	}

	// Open image file for writing
	file = fopen("test_copy.jpg", "w");
	if (!file) {
		fprintf(stderr, "error: %s\n", strerror(errno));
		return -1;
	}

	// Save image to file
	nwrite = fwrite(buffer, 1, nread, file);
	if (ferror(file) != 0) {
		fprintf(stderr, "error: failed  to read image data from file: %s\n", strerror(errno));
		goto out;
	}

	fprintf(stdout, "written %zd bytes\n", nwrite);

out:
	if (file)
		fclose(file);

	if (message)
		g_object_unref(message);

	if (session)
		g_object_unref(session);

	return error != NULL ? -1 : 0;
}

static int post()
{
	SoupSession *session = NULL;
	SoupMessage *message = NULL;
	GError *error = NULL;
	FILE *file = NULL;
	char *base64_image_data = NULL;
	char *form_data = NULL;
	gsize nread = 0;

	// Open image file for reading
	file = fopen("test_copy.jpg", "r");
	if (!file) {
		fprintf(stderr, "error: failed to open image file: %s\n", strerror(errno));
		goto out;
	}

	// Load image from file
	nread = fread(buffer, 1, sizeof(buffer), file);
	if (ferror(file) != 0) {
		fprintf(stderr, "error: failed  to read image data from file: %s\n", strerror(errno));
		goto out;
	}

	fprintf(stdout, "read %zd bytes\n", nread);

	// Base64 encode image file data
	base64_image_data = g_base64_encode((guchar *) buffer, nread);

	// Construct form data
	form_data = soup_form_encode("id", "0c008080f62efacf", "image", base64_image_data, NULL);

	// Send HTTP POST request to server
	session = soup_session_new_with_options(SOUP_SESSION_ADD_FEATURE_BY_TYPE, SOUP_TYPE_CONTENT_SNIFFER, NULL);
	message = soup_message_new("POST", "http://localhost:8080/card_returned/photo");
	soup_message_set_request(message, "application/x-www-form-urlencoded", SOUP_MEMORY_COPY, form_data, strlen(form_data));
	soup_session_send(session, message, NULL, &error);
	if (error != NULL) {
		fprintf(stderr, "error: failed to send HTTP POST request: %s\n", error->message);
		g_error_free(error);
		goto out;
	}

	fprintf(stdout, "STATUS: POST: %d\n", message->status_code);

	if (message->status_code != 200) {
		fprintf(stderr, "error: failed to upload image to server\n");
		exit(-1);
	}

out:
	if (base64_image_data)
		g_free(base64_image_data);

	if (form_data)
		g_free(form_data);

	if (file)
		fclose(file);

	if (message)
		g_object_unref(message);

	if (session)
		g_object_unref(session);

	return error != NULL ? -1 : 0;
}

int main()
{
	get();
	post();
}
